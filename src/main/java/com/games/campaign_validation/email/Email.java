package com.games.campaign_validation.email;

import java.util.List;

public class Email
{
	@Override
    public String toString() {
        return "Email [systime=" + systime + ", id=" + id + ", dest=" + dest + ", recipients=" + recipients + ", from="
                + from + ", subject=" + subject + ", content=" + content + "]";
    }

    private long systime;
	private int id;
	private String dest;
	private List< Recipient > recipients;
	private String from;
	private String subject;
	private String content;

	/**
	 * @return the systime
	 */
	public long getSystime()
	{
		return systime;
	}

	/**
	 * @param systime the systime to set
	 */
	public void setSystime( long systime )
	{
		this.systime = systime;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId( int id )
	{
		this.id = id;
	}

	/**
	 * @return the dest
	 */
	public String getDest()
	{
		return dest;
	}

	/**
	 * @param dest the dest to set
	 */
	public void setDest( String dest )
	{
		this.dest = dest;
	}

	/**
	 * @return the recipients
	 */
	public List< Recipient > getRecipients()
	{
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients( List< Recipient > recipients )
	{
		this.recipients = recipients;
	}

	/**
	 * @return the from
	 */
	public String getFrom()
	{
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom( String from )
	{
		this.from = from;
	}

	/**
	 * @return the subject
	 */
	public String getSubject()
	{
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject( String subject )
	{
		this.subject = subject;
	}

	/**
	 * @return the content
	 */
	public String getContent()
	{
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent( String content )
	{
		this.content = content;
	}
}
