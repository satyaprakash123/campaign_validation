package com.games.campaign_validation.email;

public class Recipient {

	private String email;
	private String type;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
		
	public Recipient() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Recipient(String email, String type) {
		super();
		this.email = email;
		this.type = type;
	}
	@Override
	public String toString() {
		return "Recipient [email=" + email + ", type=" + type + "]";
	}
	
}
