package com.games.campaign_validation;


import java.io.Closeable;
import java.io.IOException;
import javax.annotation.PreDestroy;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;


@Configuration
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
public class MongoConnection extends AbstractMongoClientConfiguration implements Closeable {

	@Value(value = "${mongoURI}")
	private String uri;

	@Value(value = "${upsMongoUri}")
	private String upsMongoUri;

	@Bean(name="cmsMongoTemplate")
	@Primary
	public MongoTemplate mongoTemplate() throws Exception {
		MongoClient mongoClient = MongoClients.create(uri);
		System.out.println("cms uri "+uri);
		return new MongoTemplate(mongoClient, "eds");
	}
	
	@Bean(name="upsMongoTemplate")
	public MongoTemplate upsMongoTemplate() throws Exception {
		MongoClient mongoClient = MongoClients.create(upsMongoUri);
		System.out.println("ups uri "+upsMongoUri);
		return new MongoTemplate(mongoClient, "eds");
	}
	
	@PreDestroy
	public void close() throws IOException {
	}

	@Override
	protected String getDatabaseName() {
		return "eds";
	}

}

