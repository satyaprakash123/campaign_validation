package com.games.campaign_validation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MySqlConnection {

	@Value(value = "${sqlURI}")
	private String URI;
	Connection conn;
	Statement stmt = null;
	ResultSet rs = null;

	
	public List<String> getTable(String query) throws SQLException {
		System.out.println("Making sql connection");
		List<String> smplEqTrIds=new ArrayList<String>();

		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn =DriverManager.getConnection(URI);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				String smplEqTrId=rs.getString("TRIGGER_NAME");
				String formula_HOURANGE_cmpId[]=smplEqTrId.split("_");
				if(formula_HOURANGE_cmpId.length>=3) {
				smplEqTrIds.add(formula_HOURANGE_cmpId[2]);
				}
			}
		   	
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}finally{
			rs.close();
			stmt.close();
			conn.close();
			
		}
		return smplEqTrIds;
	}
}
