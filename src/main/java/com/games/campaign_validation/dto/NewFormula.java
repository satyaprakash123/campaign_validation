package com.games.campaign_validation.dto;

public class NewFormula {
	
	private String id;
	private String name;
	private String formula;
	private String scoreType;
	private String startDate;
	private String status;
	
	public NewFormula() {
		super();
	}

	public NewFormula(String id, String name, String formula, String scoreType, String startDate, String status) {
		super();
		this.id = id;
		this.name = name;
		this.formula = formula;
		this.scoreType = scoreType;
		this.startDate = startDate;
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "NewFormula [id=" + id + ", name=" + name + ", formula=" + formula + ", scoreType=" + scoreType
				+ ", startDate=" + startDate + ", status=" + status + "]";
	}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFormula() {
		return formula;
	}


	public void setFormula(String formula) {
		this.formula = formula;
	}


	public String getScoreType() {
		return scoreType;
	}


	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
}
