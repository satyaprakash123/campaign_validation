package com.games.campaign_validation.dto;

public class UTMData {
    private String name;
    private String type;
    private Long time;

    @Override
    public String toString() {
        return name + "|" + type + "|" + time;
    }

    public UTMData(String name, String type, Long time) {
        super();
        this.name = name;
        this.type = type;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + type.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UTMData other = (UTMData) obj;
        if(name.equals(other.name) && type.equals(other.type)){
            return true;
        }
        else{
            return false;
        }
          }

}
