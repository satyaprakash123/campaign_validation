package com.games.campaign_validation.dto;

public class OfferCampaign {
private String id;
private String name;
private Long startDate;
private Long endDate;

public OfferCampaign(String id, String name, Long startDate, Long endDate) {
	super();
	this.id = id;
	this.name = name;
	this.startDate = startDate;
	this.endDate = endDate;
}
@Override
public String toString() {
	return "OfferCampaign [id=" + id + ", name=" + name + ", startDate="
			+ startDate + ", endDate=" + endDate + "]";
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Long getStartDate() {
	return startDate;
}
public void setStartDate(Long startDate) {
	this.startDate = startDate;
}
public Long getEndDate() {
	return endDate;
}
public void setEndDate(Long endDate) {
	this.endDate = endDate;
}



}
