package com.games.campaign_validation.dto;

public class CmCampaigns {
private String id;
private String name;
private String status;
private Long startDatelong;
private Long endDatelong;
private String startDate;
private String endDate;





public CmCampaigns(String id, String name, String status, Long startDatelong,
		Long endDatelong, String startDate, String endDate) {
	super();
	this.id = id;
	this.name = name;
	this.status = status;
	this.startDatelong = startDatelong;
	this.endDatelong = endDatelong;
	this.startDate = startDate;
	this.endDate = endDate;
}
public String getStartDate() {
	return startDate;
}
public String getEndDate() {
	return endDate;
}
public Long getStartDatelong() {
	return startDatelong;
}
public void setStartDatelong(Long startDatelong) {
	this.startDatelong = startDatelong;
}
public Long getEndDatelong() {
	return endDatelong;
}
public void setEndDatelong(Long endDatelong) {
	this.endDatelong = endDatelong;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
@Override
public String toString() {
	return "CmCampaigns [id=" + id + ", name=" + name + ", status=" + status
			+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
}


}
