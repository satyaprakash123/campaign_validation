package com.games.campaign_validation.dto;

public class ReCampaigns {
private String id;
private String name;
private String status;
private Long startDate;
private Long endDate;


public ReCampaigns(String id, String name, String status, Long startDate,
		Long endDate) {
	super();
	this.id = id;
	this.name = name;
	this.status = status;
	this.startDate = startDate;
	this.endDate = endDate;
}
@Override
public String toString() {
	return "ReCampaigns [id=" + id + ", name=" + name + ", status=" + status
			+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Long getStartDate() {
	return startDate;
}
public void setStartDate(Long startDate) {
	this.startDate = startDate;
}
public Long getEndDate() {
	return endDate;
}
public void setEndDate(Long endDate) {
	this.endDate = endDate;
}




}
