package com.games.campaign_validation.dto;

public class Formula {
	private String id;
	private String name;
	private String scoreType;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getScoreType() {
		return scoreType;
	}
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}
	public Formula(String id, String name, String scoreType) {
		super();
		this.id = id;
		this.name = name;
		this.scoreType = scoreType;
	}
	@Override
	public String toString() {
		return "Formula [id=" + id + ", name=" + name + ", scoreType=" + scoreType + "]";
	}
	


}
