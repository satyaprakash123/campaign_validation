package com.games.campaign_validation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import com.games.campaign_validation.dto.CmCampaigns;
import com.games.campaign_validation.dto.OfferCampaign;
import com.games.campaign_validation.dto.ReCampaigns;
import com.games.campaign_validation.dto.UTMData;

@Component
@ManagedResource(objectName = "com.games24x7:name=QuartzSchedular")
public class CampaignValidationSchedular implements ApplicationListener<ContextRefreshedEvent> {

   @Autowired
    private EmailService emailService;
    @Autowired
    private SendMail sendmail;
    @Value(value = "${emailfrom}")
    private String emailfrom; 
    @Value(value = "${emailTo}")
    private String emailTo;
    @Value(value = "${subject}")
    private String subject;
    @Value(value = "${campaignmismatch}")
    private String campaignmismatch;

    @Value(value = "${getcacheurl}")
    private String getcacheurl;

    @Value(value = "${timeduration6month}")
    private Long timeduration6month;
    @Value(value = "${utmdata:/home/tomcat/campaign_validation/logs/utmdata.log}")
    private String utmdataLog;

    @Autowired
    private MongoUtil mongoutil;
    
    
    @Autowired
    private TriggerValidation simpleEqValidation;
    
    private static Logger campaignValidationlogger = LoggerFactory.getLogger("campaignmismatch");
    private static Logger utmdata = LoggerFactory.getLogger("utmdata");
    private long FiveHr30minute = 19800000l;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        System.out.println("QuartzSchedular.onApplicationEvent()");
        try {

            List<String> ruleList = new ArrayList<String>();
            List<String> reRuleCptList = new ArrayList<String>();

            Set<Integer> utmSource_ref = new HashSet<Integer>();
            Set<String> utmSource = new HashSet<String>();
            Set<Integer> utmCampaign_ref = new HashSet<Integer>();
            Set<String> utmCampaign = new HashSet<String>();

            Set<Integer> utmTerm_ref = new HashSet<Integer>();
            Set<String> utmTerm = new HashSet<String>();

            Set<Integer> utmMedium_ref = new HashSet<Integer>();
            Set<String> utmMedium = new HashSet<String>();

            Set<Integer> utmContent_ref = new HashSet<Integer>();
            Set<String> utmContent = new HashSet<String>();

            Set<Integer> lndngPgSrc_ref = new HashSet<Integer>();
            Set<String> lndngPgSrc = new HashSet<String>();

            Map<String, CmCampaigns> cmCampaignList = mongoutil.getcmCampaigns();
            Map<String, ReCampaigns> reCampaignMap = mongoutil.getreCampaigns();
            Map<String, OfferCampaign> offerCampaignMap = mongoutil.getMapOfferCampaign();
            Map<String, Integer> getCacheCpt = getCacheCptList(getcacheurl);
            Map<String, Integer> reRuleUniqueCampainId = mongoutil.getreRuleMap(reRuleCptList, ruleList);
            List<String> formulaList=simpleEqValidation.getDeletedSimpleEqWithQuartzEntry();
             System.out.println("Deleted formulaList "+formulaList);
            for (String rule : ruleList) {
                if (rule.contains("utmSource") || rule.contains("utmCampaign") || rule.contains("utmTerm")
                        || rule.contains("utmMedium") || rule.contains("utmContent") || rule.contains("lndngPgSrc")) {
                    rule = rule.replaceAll("\\||&&", "");
                    rule = rule.replaceAll("\\(", "");
                    rule = rule.replaceAll("\\)", "");
                    rule = rule.replaceAll(",1", "");
                    rule = rule.replaceAll("\\{", "");
                    rule = rule.replaceAll("\\}", "");
                    rule = rule.replaceAll("\\   ", "");
                    rule = rule.replaceAll("\\  ", "");
                    rule = rule.replaceAll("\\  ", "");
                    rule = rule.replaceAll("\"", "");
                    String arr[] = rule.split("eq");
                    for (String a : arr) {
                        if (a.contains("utm") || a.contains("lnd")) {
                            String arr1[] = a.split("in");
                            for (String a1 : arr1) {

                                if (a1.contains("utm") || a1.contains("lnd")) {
                                    a1 = a1.replaceAll("event_", "");
                                    String arr2[] = a1.split("ne");
                                    for (String a2 : arr2) {
                                        if (a2.contains("utm") || a2.contains("lnd")) {

                                            if (a2.contains("utmSource_ref")) {
                                                try {
                                                    String utmSref[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmSref.length; i++) {
                                                        utmSource_ref.add(Integer.parseInt(utmSref[i]));
                                                    }

                                                } catch (Exception e) {
                                                    try {
                                                        if (a2.contains("lt")) {
                                                            String utmS1[] = a2.split("lt");
                                                            String utmS2[] = utmS1[0].split(",");
                                                            utmSource_ref.add(Integer.parseInt(utmS2[1]));

                                                        } else if (a2.contains("gt")) {
                                                            String utmS1[] = a2.split("gt");
                                                            String utmS2[] = utmS1[0].split(",");
                                                            utmSource_ref.add(Integer.parseInt(utmS2[1]));
                                                        }
                                                    } catch (Exception e1) {
                                                        System.out.println("Exception UTM in 2nd time" + a2 + " a1 "
                                                                + a1 + " a " + a);

                                                    }
                                                }

                                            } else if (a2.contains("utmSource")) {
                                                try {
                                                    String utmS[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmS.length; i++) {
                                                        utmSource.add(utmS[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmCampaign_ref")) {
                                                try {
                                                    String utmCref[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmCref.length; i++) {
                                                        utmCampaign_ref.add(Integer.parseInt(utmCref[i]));
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmCampaign")) {
                                                try {
                                                    String utmC[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmC.length; i++) {
                                                        utmCampaign.add(utmC[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmTerm_ref")) {
                                                try {
                                                    String utmTref[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmTref.length; i++) {
                                                        utmTerm_ref.add(Integer.parseInt(utmTref[i]));
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmTerm")) {
                                                try {
                                                    String utmT[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmT.length; i++) {
                                                        utmTerm.add(utmT[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmMedium_ref")) {
                                                try {
                                                    String utmMref[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmMref.length; i++) {
                                                        utmMedium_ref.add(Integer.parseInt(utmMref[i]));
                                                    }

                                                } catch (Exception e) {
                                                    try {
                                                        if (a2.contains("lt")) {
                                                            String utmM1[] = a2.split("lt");
                                                            String utmM2[] = utmM1[0].split(",");
                                                            utmMedium_ref.add(Integer.parseInt(utmM2[1]));
                                                        } else if (a2.contains("gt")) {
                                                            String utmM1[] = a2.split("gt");
                                                            String utmM2[] = utmM1[0].split(",");
                                                            utmMedium_ref.add(Integer.parseInt(utmM2[1]));
                                                        }
                                                    } catch (Exception e1) {
                                                        System.out
                                                                .println("Exception utmM1 in 2nd " + a2 + " a1 " + a1);
                                                    }

                                                }

                                            } else if (a2.contains("utmMedium")) {
                                                try {
                                                    String utmM[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmM.length; i++) {
                                                        utmMedium.add(utmM[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmContent_ref")) {
                                                try {
                                                    String utmConref[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmConref.length; i++) {
                                                        utmContent_ref.add(Integer.parseInt(utmConref[i]));
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("utmContent")) {
                                                try {
                                                    String utmCon[] = a2.split(",");

                                                    for (int i = 0 + 1; i < utmCon.length; i++) {
                                                        utmContent.add(utmCon[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("lndngPgSrc_ref")) {
                                                try {
                                                    String lndRef[] = a2.split(",");

                                                    for (int i = 0 + 1; i < lndRef.length; i++) {
                                                        lndngPgSrc_ref.add(Integer.parseInt(lndRef[i]));
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            } else if (a2.contains("lndngPgSrc")) {
                                                try {
                                                    String lnd[] = a2.split(",");

                                                    for (int i = 0 + 1; i < lnd.length; i++) {
                                                        lndngPgSrc.add(lnd[i]);
                                                    }

                                                } catch (Exception e) {
                                                    System.out.println("Exception in " + a2 + " a1 " + a1);

                                                }

                                            }
                                        }

                                    }
                                }

                            }

                        }

                    }

                }
            }

            int totalCountValidCampaign = 0;
            int validOffercampaignCount = 0;
            List<String> statusNullList = new ArrayList<String>();
            List<CmCampaigns> validReCamapignmissingList = new ArrayList<CmCampaigns>();
            List<CmCampaigns> validofferCampaignDatamissingList = new ArrayList<CmCampaigns>();
            List<CmCampaigns> validRe_ruleCampaignDatamissingList = new ArrayList<CmCampaigns>();

            
            int totalPauseArchivedCount = 0;
            List<CmCampaigns> pauseArchivedDataInReRuleList = new ArrayList<CmCampaigns>();
            List<CmCampaigns> pauseArchivedDataInOfferCampaignList = new ArrayList<CmCampaigns>();
            List<CmCampaigns> pauseArchivedPresentInRe_rule_and_OfferCampaignList = new ArrayList<CmCampaigns>();

            List<String> re_rule_getcacheMissingList = new ArrayList<String>();

            long now = System.currentTimeMillis();
            for (Map.Entry<String, CmCampaigns> entry : cmCampaignList.entrySet()) {
                CmCampaigns cmCampaign = entry.getValue();

                if ((cmCampaign.getStatus() == null)) {
                    statusNullList.add("Name " + cmCampaign.getName() + " Id " + cmCampaign.getId());

                }
                // case 1:valid campaign (status=schedule and now>startdate &&
                // (enddate=null||enddate>now)) //check ongoing is there or not
                // and put condition
                if ((!cmCampaign.getStatus().equalsIgnoreCase("Paused")
                        && !cmCampaign.getStatus().equalsIgnoreCase("Archived") && !cmCampaign.getStatus()
                        .equalsIgnoreCase("Completed"))
                        && (now >= cmCampaign.getStartDatelong() && (cmCampaign.getEndDate() == null || cmCampaign
                                .getEndDatelong() + FiveHr30minute >= now))) {
                    // all valid campaign
                    totalCountValidCampaign++;
                    if (reCampaignMap.get(cmCampaign.getId()) == null) {
                        validReCamapignmissingList.add(cmCampaign);

                    }
                    if (offerCampaignMap.get(cmCampaign.getId()) == null
                            && !cmCampaign.getStatus().equalsIgnoreCase("Awaiting Approval")) {
                        validofferCampaignDatamissingList.add(cmCampaign);

                    } else {
                        validOffercampaignCount++;

                    }
                    if (reRuleUniqueCampainId.get(cmCampaign.getId()) == null
                            && !cmCampaign.getStatus().equalsIgnoreCase("Awaiting Approval")) {

                        validRe_ruleCampaignDatamissingList.add(cmCampaign);
                    }

                }
                
                // case 3:end date over and status=schedule //now>startdate
                // check pause and archived
                if (((cmCampaign.getStatus().equalsIgnoreCase("Paused")
                        || cmCampaign.getStatus().equalsIgnoreCase("Archived") || cmCampaign.getStatus()
                        .equalsIgnoreCase("Completed")) || (cmCampaign.getEndDate() != null && cmCampaign
                        .getEndDatelong() + FiveHr30minute < now))) {
                    // all invalid campaign
                    totalPauseArchivedCount++;
                    if (reRuleUniqueCampainId.get(cmCampaign.getId()) != null) {
                        pauseArchivedDataInReRuleList.add(cmCampaign);
                        if (offerCampaignMap.get(cmCampaign.getId()) != null) {
                            pauseArchivedPresentInRe_rule_and_OfferCampaignList.add(cmCampaign);
                        }

                    }
                    if (offerCampaignMap.get(cmCampaign.getId()) != null) {
                        pauseArchivedDataInOfferCampaignList.add(cmCampaign);
                    }

                }

            }
            for (String cpt : reRuleCptList) {

                if (getCacheCpt.get(cpt) == null) {
                    re_rule_getcacheMissingList.add(cpt);

                }

            }
            
            Set<UTMData> listFiledata = new HashSet<UTMData>();
            try (BufferedReader bufferReader = new BufferedReader(new FileReader(
            		utmdataLog))) {
                String line = bufferReader.readLine();
                while (line != null) {
                    String array[] = line.split("\\|");
                    Long time = Long.parseLong(array[2]);
                    if (time + timeduration6month > (System.currentTimeMillis())) {
                        listFiledata.add(new UTMData(array[0], array[1], time));
                    } else {
                       // System.out.println("discard utm data " + new UTMData(array[0], array[1], time));
                    }
                    line = bufferReader.readLine();

                }
            } catch (FileNotFoundException e) {
                // Exception handling
                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }

            

           /* String text = "1) Number of campaign in collections [cm_campaign]= " + cmCampaignList.size()
                    + " [re_campaigns]= " + reCampaignMap.size() + " [offer_campaigns] = " + offerCampaignMap.size()
                    + "  [re_rules_123] = " + reRuleCptList.size() + " getCacheRule = " + getCacheCpt.size()
                    + " [re_rules_123] vs getCacheRule missing count = " + re_rule_getcacheMissingList.size() + "\n";
            text += "case 2(Running campaign):status!=Paused,Archived,Completed && Now>=startDate &&(endDate>=now ||endDate=null)" + "\n";
            text += "2a) Running campaign [cm_campaign] count " + totalCountValidCampaign + "\n";
            text += "2b)Running campaign  [cm_campaigns Vs re_campaigns] , Campign missing count "
                    + validReCamapignmissingList.size() + "\n";
            text += "2c) (and status!=Awaiting Approval) Running campaign [cm_campaigns Vs re_rules_123 ], Camapign missing count (have to exclude predefined cmp)"
                    + validRe_ruleCampaignDatamissingList.size() + "\n";
            text += "2d)Running campaign [cm_campaigns Vs offer_campaigns],  campaign match count "
                    + validOffercampaignCount + "\n";
            text += "2e)(and status!=Awaiting Approval)Running campaign [cm_campaigns Vs  offer_campaigns] , campaign missing count "
                    + validofferCampaignDatamissingList.size() + "\n";
            text += "case 3(Campaigns which are completed,Paused or Archived):status=Paused,Archived,Completed || (now>endDate && endDate!=null) " + "\n";
            text += "3a) Total count of completed campaigns according to [cm_campaigns] " + totalPauseArchivedCount + "\n";
            text += "3b) [cm_campaigns Vs re_rules_123] , campaign present count " + pauseArchivedDataInReRuleList.size()
                    + "\n";
            text += "3c) [cm_campaigns]  campaign present in [re_rules_123] and [offer_campaigns]  count "
                    + pauseArchivedPresentInRe_rule_and_OfferCampaignList.size() + "\n";
            int utmcount= utmSource_ref.size() + utmSource.size()+ utmCampaign_ref.size() + utmCampaign.size()+ utmTerm_ref.size()+ utmTerm.size()
                    + utmMedium_ref.size() + utmMedium.size()
                    + utmContent_ref.size() +  utmContent.size()
                     +  lndngPgSrc_ref.size() + lndngPgSrc.size();
            text += "4a)Total utmSource/term used in campaigns count = " +utmcount+"\n";

            text += "4b)Count [utmSource_ref] = " + utmSource_ref.size() + " [utmSource]= " + utmSource.size()
                    + " [utmCampaign_ref]= " + utmCampaign_ref.size() + " [utmCampaign]= " + utmCampaign.size()
                    + " [utmTerm_ref]= " + utmTerm_ref.size() + " [utmTerm]= " + utmTerm.size()
                    + " [utmMedium_ref]= " + utmMedium_ref.size() + " [utmMedium]= " + utmMedium.size()
                    + " [utmContent_ref]= " + utmContent_ref.size() + " [utmContent]= " + utmContent
                    + " [lndngPgSrc_ref]= " + lndngPgSrc_ref.size() + " [lndngPgSrc]= " + lndngPgSrc.size();
            
            StringBuilder sb = new StringBuilder();
            sb.append("<html><body><table>1) Number of campaign in collections [cm_campaign]= ><td>Bubu<td>Lala</tr>");
            
            sb.append("<html><body><table><tr><td>Bubu<td>cmCampaignList.size()</tr></table></body></html>");
            String text=sb.toString();
            

            campaignValidationlogger.info("1) Number of campaign in collections [cm_campaign]= " + cmCampaignList.size()
                    + " [re_campaigns]= " + reCampaignMap.size() + " [offer_campaigns] = " + offerCampaignMap.size()
                    + "  [re_rules_123] = " + reRuleCptList.size() + " getCacheRule = " + getCacheCpt.size()
                    + " [re_rules_123] vs getCacheRule missing count = " + re_rule_getcacheMissingList.size()+" data "+re_rule_getcacheMissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("case 2(Running campaign):status!=Paused,Archived,Completed && Now>=startDate &&(endDate>=now ||endDate=null)");
            campaignValidationlogger.info("2a) Running campaign [cm_campaigns] count " + totalCountValidCampaign);
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("2b)Running campaign  [cm_campaigns Vs re_campaigns] , Camapign missing count "
                            + validReCamapignmissingList.size()+" data "+validReCamapignmissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("2c) (and status!=Awaiting Approval) Running campaign [cm_campaigns Vs re_rules_123 ], Camapign missing count (have to exclude predefined cmp) "
                            + validRe_ruleCampaignDatamissingList.size()+" data "+validRe_ruleCampaignDatamissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("2d)Running campaign [cm_campaigns Vs offer_campaigns],  campaign match count "
                    + validOffercampaignCount);
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("2e)(and status!=Awaiting Approval) Running campaign [cm_campaigns Vs  offer_campaigns] , campaign missing count "
                            + validofferCampaignDatamissingList.size()+" data "+validofferCampaignDatamissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("case 3(Campaigns which are completed,Paused or Archived):status=Paused,Archived,Completed || (now>endDate && endDate!=null)");
            campaignValidationlogger.info("");
            campaignValidationlogger.info("3a) Total count of completed campaigns according to [cm_campaigns]  " + totalPauseArchivedCount);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("3b) [cm_campaigns Vs re_rules_123] , campaign present count " + pauseArchivedDataInReRuleList.size()+" data "+pauseArchivedDataInReRuleList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("3c) [cm_campaigns]  campaign present in [re_rules_123] and [offer_campaigns]  count "
                    + pauseArchivedPresentInRe_rule_and_OfferCampaignList.size()+" data "+pauseArchivedPresentInRe_rule_and_OfferCampaignList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("4b)Count [utmSource_ref] = " + utmSource_ref.size()+" data "+utmSource_ref + " [utmSource]= " + utmSource.size()+" data "+utmSource
                    + " [utmCampaign_ref]= " + utmCampaign_ref.size()+" data "+utmCampaign_ref + " [utmCampaign]= " + utmCampaign.size()+" data "+utmCampaign
                    + " [utmTerm_ref]= " + utmTerm_ref.size()+" data "+utmTerm_ref + " [utmTerm]= " + utmTerm.size()+" data "+utmTerm
                    + " [utmMedium_ref]=" + utmMedium_ref.size()+" data "+utmMedium_ref + " [utmMedium]= " + utmMedium.size()+" data "+utmMedium
                    + " [utmContent_ref]= " + utmContent_ref.size()+" [utmContent_ref]= "+utmContent_ref + " [utmContent]=  " + utmContent.size()+" data "+utmContent
                    + " [lndngPgSrc_ref]= " + lndngPgSrc_ref.size()+" data "+lndngPgSrc_ref + " [lndngPgSrc]= " + lndngPgSrc.size()+" data "+lndngPgSrc);
*/
            
            String text = "1) [re_rules_123] vs getCacheRule missing count = " + re_rule_getcacheMissingList.size() + "\n";
            text += "case 2(Running campaign):status!=Paused,Archived,Completed && Now>=startDate &&(endDate>=now ||endDate=null)" + "\n";
            text += "2a)(and status!=Awaiting Approval)Running campaign [cm_campaigns Vs  offer_campaigns] , campaign missing count "
                    + validofferCampaignDatamissingList.size() + "\n";
            text += "case 3(Campaigns which are completed,Paused or Archived):status=Paused,Archived,Completed || (now>endDate && endDate!=null) " + "\n";
            text += "3a) [cm_campaigns Vs re_rules_123] , campaign present count " + pauseArchivedDataInReRuleList.size()
                    + "\n";
            text += "3b) [cm_campaigns]  campaign present in [re_rules_123] and [offer_campaigns]  count "
                    + pauseArchivedPresentInRe_rule_and_OfferCampaignList.size() + "\n";
            
            text += "4) [QRTZ_TRIGGERS]  vs [newFormula]"
                    + formulaList.size() + "\n";
           
                  
            
            
            /*StringBuilder sb = new StringBuilder();
            sb.append("<html><body><table>1 [re_rules_123] vs getCacheRule missing count = ><td>re_rule_getcacheMissingList.size()<td></tr>");
            
            sb.append("<html><body><table><tr><td>Bubu<td>cmCampaignList.size()</tr></table></body></html>");
            String textTable=sb.toString();
            */

            campaignValidationlogger.info("1)[re_rules_123] vs getCacheRule missing count = " + re_rule_getcacheMissingList.size()+" data "+re_rule_getcacheMissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("case 2(Running campaign):status!=Paused,Archived,Completed && Now>=startDate &&(endDate>=now ||endDate=null)");
            campaignValidationlogger.info("");
            campaignValidationlogger
                    .info("2a)(and status!=Awaiting Approval) Running campaign [cm_campaigns Vs  offer_campaigns] , campaign missing count "
                            + validofferCampaignDatamissingList.size()+" data "+validofferCampaignDatamissingList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("case 3(Campaigns which are completed,Paused or Archived):status=Paused,Archived,Completed || (now>endDate && endDate!=null)");
            campaignValidationlogger.info("");
            campaignValidationlogger.info("3a) [cm_campaigns Vs re_rules_123] , campaign present count " + pauseArchivedDataInReRuleList.size()+" data "+pauseArchivedDataInReRuleList);
            campaignValidationlogger.info("");
            campaignValidationlogger.info("3b) [cm_campaigns]  campaign present in [re_rules_123] and [offer_campaigns]  count "
                    + pauseArchivedPresentInRe_rule_and_OfferCampaignList.size()+" data "+pauseArchivedPresentInRe_rule_and_OfferCampaignList);
            
            campaignValidationlogger.info("4) [QRTZ_TRIGGERS]  vs [newFormula] "+formulaList);
            
            
            
            try (FileWriter fileWriter = new FileWriter(utmdataLog)) {

                for (Integer utmS_ref : utmSource_ref) {
                    listFiledata.add(new UTMData(utmS_ref + "", "utmSource_ref", System.currentTimeMillis()));

                }
                for (String utmS : utmSource) {
                    listFiledata.add(new UTMData(utmS + "", "utmSource", System.currentTimeMillis()));

                }
                for (Integer utmC_ref : utmCampaign_ref) {
                    listFiledata.add(new UTMData(utmC_ref + "", "utmCampaign_ref", System.currentTimeMillis()));

                }
                for (String utmC : utmCampaign) {
                    listFiledata.add(new UTMData(utmC + "", "utmCampaign", System.currentTimeMillis()));

                }
                for (Integer utmT_ref : utmTerm_ref) {
                    listFiledata.add(new UTMData(utmT_ref + "", "utmTerm_ref", System.currentTimeMillis()));

                }
                for (String utmT : utmTerm) {
                    listFiledata.add(new UTMData(utmT + "", "utmTerm", System.currentTimeMillis()));

                }
                for (Integer utmM_ref : utmMedium_ref) {
                    listFiledata.add(new UTMData(utmM_ref + "", "utmMedium_ref", System.currentTimeMillis()));

                }
                for (String utmM : utmMedium) {
                    listFiledata.add(new UTMData(utmM + "", "utmMedium", System.currentTimeMillis()));

                }
                for (Integer utmCont_ref : utmContent_ref) {
                    listFiledata.add(new UTMData(utmCont_ref + "", "utmContent_ref", System.currentTimeMillis()));

                }
                for (String utmCont : utmContent) {
                    listFiledata.add(new UTMData(utmCont + "", "utmContent", System.currentTimeMillis()));

                }
                for (Integer lndp_ref : lndngPgSrc_ref) {
                    listFiledata.add(new UTMData(lndp_ref + "", "lndngPgSrc_ref", System.currentTimeMillis()));

                }
                for (String lndp : lndngPgSrc) {
                    listFiledata.add(new UTMData(lndp + "", "lndngPgSrc", System.currentTimeMillis()));
                }
                for (UTMData utm : listFiledata) {
                    fileWriter.write(utm.toString() + "\n");

                }

                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
                // Cxception handling
            }

            //System.out.println("listFiledata size " + listFiledata.size() + "data " + listFiledata);

            List<String> emaillist = new ArrayList<String>();
            String arrayEmail[] = emailTo.split(",");
            for (int i = 0; i < arrayEmail.length; i++) {
                emaillist.add(arrayEmail[i]);
            }

            if(re_rule_getcacheMissingList.size()>0||validofferCampaignDatamissingList.size()>0||pauseArchivedDataInReRuleList.size()>0){
           sendmail.sendEmail(emaillist, emailfrom, subject, text, campaignmismatch);
            System.exit(0);}
            else{
                System.out.println("no issue ,no need to send mail");
            }
            System.out.println("stop server");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Map<String, Integer> getCacheCptList(String url) throws Exception {
        Map<String, Integer> cacheCptList = new HashMap<String, Integer>();
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String response = new String();
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    response = inputLine;

                }
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(response);
                Set<String> keys = json.keySet();

                for (String key : keys) {
                    JSONObject object = (JSONObject) json.get(key);
                    Set<String> set2 = object.keySet();
                    for (String cpt : set2) {
                        cacheCptList.put(cpt.toString(), 1);
                    }

                }
                in.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cacheCptList;
    }

}
