package com.games.campaign_validation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;

import javax.annotation.PostConstruct;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Service;

@Service
public class SendMail {
	Session session=null;
	//Get the session object  
	Properties properties=null;
	
	@PostConstruct
	public void init(){
	
		  properties = System.getProperties();  
		    properties.setProperty("mail.smtp.host", "localhost");  
		     session = Session.getDefaultInstance(properties);  

	}
   

	public void sendEmail(List<String> emailList,String fromemail,String Subject,String text,String file) throws IOException {
		  
	    //String from = "no.reply@games24x7.com";//change accordingly  
	   
	   
	   //compose the message  
	    try{  
	       MimeMessage message = new MimeMessage(session);  
	       message.setFrom(new InternetAddress(fromemail));  
	       for (int i = 0; i < emailList.size(); i++) {
	    	   message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailList.get(i)));	
		}
	       message.setSubject(Subject);  
	       message.setText(text);  
	       
	       
	       //new code start
	       // Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();

	         // Now set the actual message
	         messageBodyPart.setText(text);

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	         	         MimeBodyPart attachPart = new MimeBodyPart();
	         attachPart.attachFile(file);
	         attachPart.setFileName("campaignmismatch.log");
	         multipart.addBodyPart(attachPart);
	         	         // Send the complete message parts
	         message.setContent(multipart);
	         	       Transport.send(message);  
	       System.out.println("message sent successfully....");  

	    }catch (MessagingException mex) {mex.printStackTrace();}  
	     
	}
	public void gzipFile(String source_filepath, String destinaton_zip_filepath) {
		 
        byte[] buffer = new byte[1024];
 
        try {
             
            FileOutputStream fileOutputStream =new FileOutputStream(destinaton_zip_filepath);
 
            GZIPOutputStream gzipOuputStream = new GZIPOutputStream(fileOutputStream);
 
            FileInputStream fileInput = new FileInputStream(source_filepath);
 
            int bytes_read;
             
            while ((bytes_read = fileInput.read(buffer)) > 0) {
                gzipOuputStream.write(buffer, 0, bytes_read);
            }
 
            fileInput.close();
 
            gzipOuputStream.finish();
            gzipOuputStream.close();
 
            System.out.println("The file was compressed successfully!");
 
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

	static void compress(String inputfile,String outputfile) 
    { 
        byte[] buffer = new byte[1024]; 
        try
        { 
            GZIPOutputStream os =  
                    new GZIPOutputStream(new FileOutputStream(outputfile)); 
                      
            FileInputStream in = 
                    new FileInputStream(inputfile); 
              
            int totalSize; 
            while((totalSize = in.read(buffer)) > 0 ) 
            { 
                os.write(buffer, 0, totalSize); 
            } 
              
            in.close(); 
            os.finish(); 
            os.close(); 
              
            System.out.println("File Successfully compressed"); 
        } 
        catch (IOException e) 
        { 
            e.printStackTrace(); 
        } 
          
    }

	private void compressFile(String file) throws FileNotFoundException, IOException {
		FileInputStream fis=new FileInputStream(file); 
		 
		 //Assign compressed file:file2 to FileOutputStream 
		 FileOutputStream fos=new FileOutputStream("/home/tomcat/data_sanity/mismatchzip"); 
		 
   
		 //Assign FileOutputStream to DeflaterOutputStream 
		 DeflaterOutputStream dos=new DeflaterOutputStream(fos); 
   
		 //read data from FileInputStream and write it into DeflaterOutputStream 
		 int data; 
		 while ((data=fis.read())!=-1) 
		 { 
		     dos.write(data); 
		 } 
   
		 //close the file 
		 fis.close(); 
		 dos.close();
	}
	
}
