package com.games.campaign_validation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.games.campaign_validation.dto.CmCampaigns;
import com.games.campaign_validation.dto.NewFormula;
import com.games.campaign_validation.dto.OfferCampaign;
import com.games.campaign_validation.dto.ReCampaigns;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;

@Service
public class MongoUtil {
	private static Logger logger = LoggerFactory.getLogger(MongoUtil.class);

	private static String CM_CAMPAIGNS = "cm_campaigns";
	private static String RE_CAMPAIGNS = "re_campaigns";
	private static String offer_campaigns = "offer_campaigns";
	private static String re_rules_123 = "re_rules_123";
	private static String NEW_FORMULA = "newFormula";

	@Autowired
	@Qualifier("upsMongoTemplate")
	private MongoTemplate mongoTemplate;

	public Map<String, CmCampaigns> getcmCampaigns() {
		Map<String, CmCampaigns> cmCampaignMap = new HashMap<String, CmCampaigns>();
		try {
			BasicDBObject query = new BasicDBObject();

			FindIterable<Document> cm_campaigns = mongoTemplate.getCollection(CM_CAMPAIGNS).find(query);

			for (Document doc : cm_campaigns) {
				System.out.print("doc "+doc);

				Long startDate = null;
				Long endDate = null;
				String status = doc.getString("status");

				if (doc.getString("startDate") != null) {

					startDate = getTimeFromDate(doc.getString("startDate"), doc.getString("name"));

				}
				if (doc.getString("endDate") != null) {
					endDate = getTimeFromDate(doc.getString("endDate"), doc.getString("name"));

				}
				cmCampaignMap.put(doc.getObjectId("_id").toString(),
						new CmCampaigns(doc.getObjectId("_id").toString(), doc.getString("name"), status, startDate,
								endDate, doc.getString("startDate"), doc.getString("endDate")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cmCampaignMap;
	}

	public Map<String, ReCampaigns> getreCampaigns() {
		Map<String, ReCampaigns> reCampaignMap = new HashMap<String, ReCampaigns>();
		try {
			BasicDBObject query = new BasicDBObject();

			FindIterable<Document> cm_campaigns = mongoTemplate.getCollection(RE_CAMPAIGNS).find(query);
			for (Document doc : cm_campaigns) {

				Long startDate = null;
				Long endDate = null;
				String status = doc.getString("status");

				if (doc.getString("startDate") != null) {

					startDate = getTimeFromDate(doc.getString("startDate"), doc.getString("name"));

				}
				if (doc.getString("endDate") != null) {
					endDate = getTimeFromDate(doc.getString("endDate"), doc.getString("name"));

				}
				reCampaignMap.put(doc.getObjectId("_id").toString(), new ReCampaigns(doc.getObjectId("_id").toString(),
						doc.getString("name"), status, startDate, endDate));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return reCampaignMap;
	}

	public static Long getTimeFromDate(Object object, String name) {
		SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		datetimeFormatter1.setTimeZone(TimeZone.getTimeZone("IST"));
		String date = null;
		Date dateObject;
		try {
			if (object != null && !object.equals("")) {
				date = String.valueOf(object);
				dateObject = datetimeFormatter1.parse(date);
				return dateObject.getTime();
			}
		} catch (Exception e) {

			return getTimeddMMYYYYformat(date, name);

		}
		return null;
	}

	public static Long getTimeddMMYYYYformat(String str, String name) {
		try {
			SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

			datetimeFormatter1.setTimeZone(TimeZone.getTimeZone("IST"));

			String arr[] = str.split("\\+");
			Date dateObject = null;
			try {
				dateObject = datetimeFormatter1.parse(arr[0]);
				return dateObject.getTime();

			} catch (Exception e) {

				System.out.println("Error ");
			}

		} catch (Exception e) {
			logger.error(" getTimeddMMYYYYformat " + str + " name " + name);
		}
		return null;
	}

	public Map<String, OfferCampaign> getMapOfferCampaign() {
		Map<String, OfferCampaign> offerCampaignMap = new HashMap<String, OfferCampaign>();
		try {
			BasicDBObject query = new BasicDBObject();

			FindIterable<Document> offerCampaign = mongoTemplate.getCollection(offer_campaigns).find(query);
			for (Document doc : offerCampaign) {

				Long startDate = null;
				Long endDate = null;
				if (doc.getString("startDate") != null) {

					startDate = getTimeFromDate(doc.getString("startDate"), doc.getString("name"));

				}
				if (doc.getString("endDate") != null) {
					endDate = getTimeFromDate(doc.getString("endDate"), doc.getString("name"));

				}
				offerCampaignMap.put(doc.getObjectId("_id").toString(), new OfferCampaign(
						doc.getObjectId("_id").toString(), doc.getString("name"), startDate, endDate));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerCampaignMap;
	}

	public Map<String, Integer> getreRuleMap(List<String> reRuleCptList, List<String> ruleList) {
		Map<String, Integer> reruleMap = new HashMap<String, Integer>();
		try {
			BasicDBObject query = new BasicDBObject();

			FindIterable<Document> offerCampaign = mongoTemplate.getCollection(re_rules_123).find(query);
			for (Document doc : offerCampaign) {
				reRuleCptList.add(doc.getString("cpt"));

				reruleMap.put(doc.getString("cmpId"), 1);
				ruleList.add(doc.getString("rule"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return reruleMap;

	}

	public Map<String, NewFormula> getNewFormula() {
		Map<String, NewFormula> newFormulaMap = new HashMap<String, NewFormula>();
		try {
			BasicDBObject query = new BasicDBObject();
			FindIterable<Document> newFormula = mongoTemplate.getCollection(NEW_FORMULA).find();
			for(Document doc:newFormula) {
				Long startDate = null;
				String status = doc.getString("status");
				if (doc.getString("startDate") != null) {
					startDate = getTimeFromDate(doc.getString("startDate"), doc.getString("name"));
					newFormulaMap.put(doc.getObjectId("_id").toString(),
							new NewFormula(doc.getObjectId("_id").toString(), doc.getString("name"),
									doc.getString("formula"), doc.getString("scoreType"), doc.getString("startDate"),
									doc.getString("status")));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newFormulaMap;
	}

}
