package com.games.campaign_validation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.games.campaign_validation.dto.Formula;
import com.mongodb.client.FindIterable;

@Service
public class TriggerValidation {

	@Autowired
	@Qualifier("cmsMongoTemplate")
	MongoTemplate mongoTemplate;
	@Autowired
	private MySqlConnection mysqlConn;
	@Value(value = "${simpleEqnQuartzQuery:select * from QRTZ_TRIGGERS where TRIGGER_NAME like '%formula_HOURANGE_*%'}")
	private String simpleEqnQuartzQuery;
	   
	@Autowired
	@Qualifier("upsMongoTemplate")
	MongoTemplate upsMongoTemplate;
	
	
	
	public List<String> getDeletedSimpleEqWithQuartzEntry() throws SQLException {
		
		 Map<String,Formula> formulaMap=new HashMap<String,Formula>();
		 List<String> formulaListFilter=new ArrayList<String>();
		 try {
	
		 List<String> smplQuartzIds=mysqlConn.getTable(simpleEqnQuartzQuery);
		 if(smplQuartzIds!=null && smplQuartzIds.size()>0) {
				FindIterable<Document> newFormula=upsMongoTemplate.getCollection("newFormula").find();
				for(Document doc:newFormula) {
					if(doc.get("status")!=null && doc.get("status").toString().equalsIgnoreCase("Active")) {
						formulaMap.put(doc.get("_id").toString(),new Formula(doc.get("_id").toString(),doc.get("name").toString(),doc.get("scoreType").toString()));
					}
				
				}
				 System.out.println(" smplQuartzIds "+smplQuartzIds+"\n"+" formulaMap "+formulaMap );

				for(String id:smplQuartzIds) {
					if(!formulaMap.containsKey(id)) {
						formulaListFilter.add(id);
						
					}
					
				}
		 }
		 }
		 catch(Exception e) {
			 e.printStackTrace();
		 }

	
		return formulaListFilter;
	}
	
}
