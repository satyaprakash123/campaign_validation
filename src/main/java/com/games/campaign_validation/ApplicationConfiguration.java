package com.games.campaign_validation;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("file:/home/tomcat/campaign_validation/src/main/resources/config.properties")
public class ApplicationConfiguration {

}
